import pyopencl as cl  # Import the OpenCL GPU computing API
import pyopencl.array as cl_array  # Import PyOpenCL Array (a Numpy array plus an OpenCL buffer object)
from pyopencl.reduction import ReductionKernel
from pyopencl.elementwise import ElementwiseKernel
import numpy  # Import Numpy number tools
from math import sqrt
import time

#Init:
context = cl.create_some_context()  # Initialize the Context
queue = cl.CommandQueue(context)  # Instantiate a Queue
#kernel functions for work:
sum1 = ReductionKernel(context, numpy.float32, neutral="0",
                        reduce_expr="a+b", map_expr="x[i]",
                        arguments="__global float *x") #reduce(lambda el1,el2:el1+el2,array)
f2 = ElementwiseKernel(context,
                        "float *x, float av, float *c",
                        "c[i] = x[i] - av", "f2") #map(lambda el1: el1-av,array)
f3 = ElementwiseKernel(context,
                        "float *x, float *y, float *c",
                        "c[i] = x[i]*y[i]", "f3") #map(lambda el1,el2: el1*el2,array1,arrat2)
#basic function:
def LCC(array1,array2):
        sx1 = sum1(array1).get()
        x_average = sx1/len(array1) #x_average
        sy1 = sum1(array2).get()
        y_average = sy1/len(array2) #y_average
        x_n1 = cl_array.empty_like(array1) #x_n1
        f2(array1, x_average, x_n1)
        y_n1 = cl_array.empty_like(array2) #y_n1
        f2(array2, y_average, y_n1) 
        xy_n1 = cl_array.empty_like(array1) #xy_n1
        f3(array1, array2, xy_n1)
        numerator = sum1(xy_n1).get() #numerator
        xx_n1 = cl_array.empty_like(array1) #xx_n1
        f3(array1, array1, xx_n1)
        yy_n1 = cl.array.empty_like(array2) #yy_n1
        f3(array2, array2, yy_n1)
        xx_sum1 = sum1(xx_n1).get() #xx_sum1
        yy_sum1 = sum1(yy_n1).get() #yy_sum1
        xxyy_d1 = xx_sum1 * yy_sum1 #xxyy_d1
        denominator = sqrt(xxyy_d1)
        return numerator/denominator
def LCC_Range(pathfile, arr1, arr2, step, lag_min, lag_max):
        len_third = int(len(arr1)/3)
        len_full = int(len_third*3)
        arr1_fix = arr1[len_third:len_third*2]
        arr2_t = lambda t: [] if (t+len_third) > len_full else  arr2[t:t+len_third]
        
        lag_range = range(lag_min,lag_max+1)
        len_lagRange = len(lag_range)
        q = 0
        for lag in lag_range:
                t = len_third+lag*step
                write(pathfile,"lag={}\t LCC={}".format(lag,str(LCC(arr1_fix,arr2_t(t)))))
                consoleStatus(progress_bar(q,len_lagRange,50))
                q+=1
#textfile
def write(pathfile,text):
        f = open(pathfile,'a')
        f.write(text+'\n')
        f.close()
def clearFile(pathfile):
        f = open(pathfile,'w')
        f.close()
def progress_bar(iteration, total, barLength):
        percent = int(round((iteration/total)*100))
        nb_bar_fill = int(round((barLength*percent)/100))
        bar_fill = '#'*nb_bar_fill
        bar_empty = ' ' * (barLength-nb_bar_fill)
        return ('\r [{0}] {1}%'.format(str(bar_fill+bar_empty),percent))
def consoleStatus(text):
        print('\r',end=text)

#data:
# cl_array.arange(queue, 4000,dtype=numpy.float32)
quantity_el = 100000
#
arr1 = cl_array.to_device(queue, numpy.random.rand(quantity_el).astype(numpy.float32))  # Create a random pyopencl array
arr2 = arr1  # Create a random pyopencl array
#
len_third = int(len(arr1)/3)
len_full = int(len_third*3)
#
pathfile1 = 'textfile.txt'
#
def main():
        clearFile(pathfile1)
        print('Допустимое значение шага: [1;{}]'.format(str(len_third)))
        step = int(input('step: '))
        while(step < 1 or step > len_third):
                print('\tОшибка. Проверьте введенное значение!')
                print('\tДопустимое значение шага: [1;{}]'.format(str(len_third)))        
                step = int(input('step: '))
        print('Допустимый диапазон значений лага: [-{0};{0}]'.format(int(len_full/(3*step))))
        lag_min = int(input('Введите минимальный лаг: '))
        while(lag_min < -int(len_full/(3*step)) or lag_min > int(len_full/(3*step))):
                print('\tОшибка. Проверьте введенное значение!')
                print('\tДопустимый диапазон значений лага: [-{0};{0}]'.format(int(len_full/(3*step))))
                lag_min = int(input('Введите минимальный лаг: '))
        lag_max = int(input('Введите максимальный лаг: '))
        while(lag_max < -int(len_full/(3*step)) or lag_max> int(len_full/(3*step))):
                print('\tОшибка. Проверьте введенное значение!')
                print('\tДопустимый диапазон значений лага: [-{0};{0}]'.format(int(len_full/(3*step))))
                lag_max = int(input('Введите минимальный лаг: '))
        start_timer = time.time()
        print('Timer: on')
        LCC_Range(pathfile1,arr1,arr2,step,lag_min,lag_max)
        time_working = time.time()-start_timer
        print('\nTimer: stop; time: {} seconds'.format(round(time_working,3)))

main()