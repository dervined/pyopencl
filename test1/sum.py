from functools import reduce
import pyopencl as cl  # Import the OpenCL GPU computing API
import pyopencl.array as cl_array  # Import PyOpenCL Array (a Numpy array plus an OpenCL buffer object)
import numpy  # Import Numpy number tools
from math import sqrt
from pyopencl.reduction import ReductionKernel
context = cl.create_some_context()  # Initialize the Context
queue = cl.CommandQueue(context)  # Instantiate a Queue

x = cl_array.to_device(queue, numpy.random.rand(500000).astype(numpy.float32))  # Create a random pyopencl array
y = cl_array.to_device(queue, numpy.random.rand(500000).astype(numpy.float32))  # Create a random pyopencl array


sum1 = ReductionKernel(context, numpy.float32, neutral="0", reduce_expr="a+b", map_expr="x[i]", arguments="__global float *x")
f2 = cl.elementwise.ElementwiseKernel(context, "float *x, float av, float *c", "c[i] = x[i] - av", "f2")
f3 = cl.elementwise.ElementwiseKernel(context, "float *x, float *y, float *c", "c[i] = x[i]*y[i]", "f3")

# Create an elementwise kernel object
#  - Arguments: a string formatted as a C argument list
#  - Operation: a snippet of C that carries out the desired map operatino
#  - Name: the fuction name as which the kernel is compiled

sx1 = sum1(x).get()
x_average = sx1/len(x) #x_average

sy1 = sum1(y).get()
y_average = sy1/len(y) #y_average

x_n1 = cl_array.empty_like(x) #x_n1
f2(x, x_average, x_n1)
y_n1 = cl_array.empty_like(y) #y_n1
f2(y, y_average, y_n1) 

xy_n1 = cl_array.empty_like(x) #xy_n1
f3(x, y, xy_n1)

numerator = sum1(xy_n1).get() #numerator

xx_n1 = cl_array.empty_like(x) #xx_n1
f3(x, x, xx_n1)

yy_n1 = cl.array.empty_like(y) #yy_n1
f3(y, y, yy_n1)

xx_sum1 = sum1(xx_n1).get() #xx_sum1

yy_sum1 = sum1(yy_n1).get() #yy_sum1

xxyy_d1 = xx_sum1 * yy_sum1 #xxyy_d1

denominator = sqrt(xxyy_d1)

res = numerator/denominator

print("x: {}".format(x))
print("y: {}".format(y))

print("res: {}".format(res))

# Print all three arrays, to show sum() worked