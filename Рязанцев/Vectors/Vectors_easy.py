import time
from random import random

#Данные
quantity_el = 3000000
arr1 = [random() for i in range(quantity_el)]
arr2 = [random() for i in range(quantity_el)]
#Методы
#Скалярное произведение
def scalarProduct(array1,array2):
    return sum(list(map(lambda el1,el2: el1*el2, array1,array2)))
#Поиск максимального значения
def maxValue(array1):
    return max(array1)
#
def main():
    start_timer = time.time()
    #Скалярное произведение двух векторов
    #result = scalarProduct(arr1,arr2)
    #Поиск максимального значения вектора
    result = maxValue(arr1)
    #
    time_working = time.time()-start_timer
    #
    print('Timer: stop; time: {} seconds'.format(round(time_working,3)))
    print('Result: {}'.format(result))

main()