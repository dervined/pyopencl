import pyopencl as cl  # Import the OpenCL GPU computing API
import pyopencl.array as cl_array  # Import PyOpenCL Array (a Numpy array plus an OpenCL buffer object)
from pyopencl.reduction import ReductionKernel
from pyopencl.elementwise import ElementwiseKernel
import numpy  # Import Numpy number tools
from math import sqrt
import time

#Init:
context = cl.create_some_context()  # Initialize the Context
queue = cl.CommandQueue(context)  # Instantiate a Queue
#kernel functions for work:
sum1 = ReductionKernel(context, numpy.float32, neutral="0",
                        reduce_expr="a+b", map_expr="x[i]",
                        arguments="__global float *x") #reduce(lambda el1,el2:el1+el2,array)
mul1 = ElementwiseKernel(context,
                        "float *x, float *y, float *c",
                        "c[i] = x[i]*y[i]", "f3") #map(lambda el1,el2: el1*el2,array1,arrat2)

#data:
# Методы добавления данных в массив numpy.array
# numpy.append(arr, arr[0])
# numpy.concatenete(arr1,arr2)
# np.take(arr, range(0,len(arr)+1), mode='wrap')
quantity_el = 3000000
#
arr1 = cl_array.to_device(queue, numpy.random.rand(quantity_el).astype(numpy.float32))  # Create a random pyopencl array
arr2 = cl_array.to_device(queue, numpy.random.rand(quantity_el).astype(numpy.float32))
#

#MainFunc
def scalarProduct(array1,array2):
    xy_arr = cl_array.empty_like(array1)
    mul1(array1,array2,xy_arr)
    result = sum1(xy_arr)
    return result
def maxValue(array1):
    return cl_array.max(array1, queue=queue)

#main
def main():
    start_timer = time.time()
    #code
    # result = scalarProduct(arr1,arr2)
    result = maxValue(arr1)
    #
    time_working = time.time()-start_timer
    print('Timer: stop; time: {} seconds'.format(round(time_working,3)))
    print('Result: {}'.format(result))

main()