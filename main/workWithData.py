from datetime import datetime,time,date

#Получение dateTime строчки
def getDateTime(line):
    line_lst = line.split('\t')
    dateAndTime = line_lst[0].split(' ')
    date_lst = list(map(lambda el1: int(el1), dateAndTime[0].split('.')))
    time_lst = list(map(lambda el1: int(el1),dateAndTime[1].split(':')))
    date1 = date(date_lst[2],date_lst[1],date_lst[0])
    time1 = time(time_lst[0],time_lst[1])
    return datetime.combine(date1,time1)
#Сравнение dateTime
def equalDateTime(dateTime1,dateTime2):
    return dateTime1 == dateTime2
#Получение два обработанных файла с данными, у которых дата совпадает
def getData(file1,file2):
    res_file1 = open('res_'+file1.name,'w')
    res_file2 = open('res_'+file2.name,'w')
    for line1 in file1:
        file2.close()
        file2 = open(file2.name)
        dateTimefile1 = getDateTime(line1)
        for line2 in file2:
            dateTimefile2 = getDateTime(line2)
            if equalDateTime(dateTimefile1,dateTimefile2):
                res_file1.write(line1)
                res_file2.write(line2)
                break
    file1.close()
    file2.close()
    res_file1.close()
    res_file2.close()
#Функция запуска приложения
def main():
    RAD = open('RAD.txt')
    SRS = open('SRS.txt')
    getData(RAD,SRS)
    print('completed')

#Запуск приложения
main()