import matplotlib.pyplot as plt

def getTextDate(pathToFile, separate):
    file1 = open(pathToFile, 'r')
    lags = []
    values = []
    for line in file1:
        lineProc = line.split(separate)
        lags.append(int(lineProc[0]))
        values.append(float(lineProc[1]))
    file1.close()
    return [lags,values]

#const
fileName = 'textfile.txt'

#main
data = getTextDate(fileName,'\t')

#draw
plt.subplot(1,1,1)
plt.plot(data[0],data[1])
plt.title('Коэффициенты корреляции')
plt.show()